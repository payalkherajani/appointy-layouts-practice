import { Layout } from '@saastack/layouts'
import React from 'react'

const TwoColumnLayoutComponent = () => {
    return (
        <Layout
            type="TwoColumnLayout"
            header={'I am two column layout header'}
            col1={'I am col1'}
            col2={'I am col2'}
            drawer={true}
            // onDrawerChange={() => console.log("this runs on window resize, basically either closes or opens drawer")}
        />
    )
}

export default TwoColumnLayoutComponent


//IMPORTANT NOTE:  const drawerOpen = col1 && !col3 ? drawer.current : false 
// For TwoColumnLayout, if you don;t need to provide col3 ,otherwise won't show drawer
