import { Layout } from '@saastack/layouts'
import React from 'react'

const ThreeColumnLayoutComponent = () => {
    return (
        <Layout
            header={'I am Header of three col layout'}
            col1={"I am col1"}
            col2={'I am col2'}
            col3={'I am col3'}
            type="ThreeColumnLayout"
        />
    )
}

export default ThreeColumnLayoutComponent


//play: comment col3 and see it changes to twoColumnLayout
