import React from 'react'
import { Layout } from '@saastack/layouts'
import Component1 from '../components/Component1'

const NoColumnLayoutComponent = () => {
    return <Layout type="NoColumnLayout" header={"I am No Column Layout Header"} col1={"I am col1"} col2={"I am Col2"} col3={"I am col3"} />
}

export default NoColumnLayoutComponent
