import { Layout } from '@saastack/layouts'
import React from 'react'

function OneColumnLayout() {
    return (
        <Layout header={"I am header of one column layout"} col1={"I am col1"} col2={"I am col2"} col3={"I am Col3"}></Layout>
    )
}
export default OneColumnLayout