import React, { useState } from 'react'
import   { Layout }  from '@saastack/layouts'
import ConfirmContainer from '@saastack/layouts/containers/ConfirmContainer'
import { Button } from '@material-ui/core'
 
const ConfirmContainerComponent = () => {

    const [open,setOpen] = useState(false)

    return(
      <div>
         <Layout
            type="NoColumnLayout" 
            header={"I am No Column Layout Header"} 
            col1={"I am col1"} 
            col2={"I am Col2"} 
            col3={"I am col3"}
         > 
         <Button onClick={() => setOpen(true)}>Delete</Button>
         {
             open && <ConfirmContainer 
               open={open}
               onClose={() => setOpen(false)}
               onAction={() => {
                console.log("yes was clicked")
                setOpen(false)
               }}
             />
         }
         </Layout>
      </div>
    )
}
export default ConfirmContainerComponent;