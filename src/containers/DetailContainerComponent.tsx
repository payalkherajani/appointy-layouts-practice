import React, { useState } from 'react'
import { Layout }  from '@saastack/layouts'
import DetailContainer from '@saastack/layouts/containers/DetailContainer'
import { Paper } from '@material-ui/core'



const DetailContainerComponent = () => {

    const [open,setOpen] = useState(true)
   
    const header = <Paper title="New Staff" />
   
    return(
       <Layout
         type="NoColumnLayout"
         header={"I am layout header"}
         col1={"I am Col1"}
         col2={<DetailContainer 
            header={header}
            open={open}
            onClose={() => setOpen(false)} 
           
          />}
       >
       </Layout>
    )
}
export default DetailContainerComponent