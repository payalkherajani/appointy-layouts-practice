import React from 'react'
import Wrapper from './Wrapper'
import NoColumnLayoutComponent from '../layouts/NoColumnLayoutComponent'
import OneColumnLayoutComponent from '../layouts/OneColumnLayoutComponent'
import TwoColumnLayoutComponent from '../layouts/TwoColumnLayoutComponent'
import ThreeColumnLayoutComponent from '../layouts/ThreeColumnLayoutComponent'
import ConfirmContainerComponent from '../containers/ConfirmContainerComponent'
import DetailContainerComponent from '../containers/DetailContainerComponent'

// A story captures the rendered state of a UI component. 
//It’s a function that returns a component’s state given a set of arguments.


export default {
    title: 'Layouts Testing',
    decorators:[
        (storyFunc: () => JSX.Element) => {
            return (
                <Wrapper>{storyFunc()}</Wrapper>
            )
        }
    ]
}

export const Default = () => <NoColumnLayoutComponent />

export const OneColumn = () => <OneColumnLayoutComponent />

export const TwoColumn = () => <TwoColumnLayoutComponent />

export const ThreeColumn = () => <ThreeColumnLayoutComponent />

export const ConfirmContainerComp = () => <ConfirmContainerComponent />

export const DetailContainerComp = () => <DetailContainerComponent />